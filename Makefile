SHELL := /bin/bash

# Colored Output
# -----------------------------------------------------------------------------
RED=\033[0;31m
GREEN=\033[0;32m
BLUE=\033[0;34m
NC=\033[0m # No Color

# Variables definitions
# -----------------------------------------------------------------------------

ifeq ($(TIMEOUT),)
TIMEOUT := 60
endif

# Default target
# -----------------------------------------------------------------------------
.DEFAULT_GOAL := help

# Target section and Global definitions
# -----------------------------------------------------------------------------
.PHONY: help all clean test install run deploy down

help:  ## Display this help.
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n%s\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

all: ## Run all
	clean test install run deploy down

install: generate_dot_env ## Install dependencies using Poetry.
	pip install --upgrade pip
	@echo "${BLUE}Installing project dependencies with Poetry...${NC}"
	pip install pipx
	pipx install poetry
	@echo "${BLUE}Configuring virtual envs in project${NC}"
	poetry config virtualenvs.in-project true
	poetry install

run: ## Start app
	PYTHONPATH=app/ poetry run uvicorn src.main:app --reload --host 0.0.0.0 --port 8080

migrate: ## Run alembic upgrade
	poetry run alembic upgrade head

deploy: generate_dot_env ## Deploy the app
	docker-compose build
	docker-compose up -d

up: ## Docker-compose up
	docker-compose up

down:## Docker-compose down
	docker-compose down

generate_dot_env: ## Generate .dotenv
	@if [[ ! -e .env ]]; then \
		cp .env.example .env; \
	fi

clean: ## Cleanup repository
	@find . -name '*.pyc' -exec rm -rf {} \;
	@find . -name '__pycache__' -exec rm -rf {} \;
	@find . -name 'Thumbs.db' -exec rm -rf {} \;
	@find . -name '*~' -exec rm -rf {} \;
	rm -rf .cache
	rm -rf build
	rm -rf dist
	rm -rf *.egg-info
	rm -rf htmlcov
	rm -rf .tox/
	rm -rf docs/_build

precommit: ## Run pre-commit checks on all files.
	poetry run pre-commit run --all-files

test:
	poetry run pytest tests -vv --show-capture=all

coverage: ## Run pytest-coverage.
	poetry run pytest --cov
