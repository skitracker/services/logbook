from fastapi import FastAPI

from src.api.routers import skitours
from src.db.database import engine
from src.db.models import metadata
from src.middleware import LogMiddleware

app = FastAPI()
app.add_middleware(LogMiddleware)


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(metadata.create_all)


# metadata.create_all(bind=engine)

app.include_router(skitours)
