from uuid import UUID

from sqlalchemy import text
from sqlalchemy.exc import DBAPIError, SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from src.api.schemas import NewSkiTourSchema, UpdatedSkiTourSchema

from .logger import get_logger

logger = get_logger(__name__)


async def fetch_all(db: AsyncSession, skip: int = 0, limit: int = 100):
    query = text(
        """
        SELECT ID, USER_ID, DATE, LOCATION, ELEVATION_GAIN, DIFFICULTY, DESCRIPTION
        FROM SKITOURS
        ORDER BY DATE DESC
        OFFSET :skip LIMIT :limit
        """
    )
    try:
        result = await db.execute(query, {"skip": skip, "limit": limit})
        entries = result.fetchall()
        return entries
    except DBAPIError as db_err:
        logger.error("Database connection or execution failed:", db_err)
        raise db_err
    except SQLAlchemyError as exc:
        logger.error("An SQLAlchemy error occurred:", exc)
        raise exc


async def fetch_one(db: AsyncSession, skitour_uid: UUID):
    query = text(
        """
        SELECT ID, USER_ID, DATE, LOCATION, ELEVATION_GAIN, DIFFICULTY, DESCRIPTION
        FROM SKITOURS
        WHERE ID = :skitour_uid
        """
    )
    try:
        result = await db.execute(query, {"skitour_uid": skitour_uid})
        return result.fetchone()
    except DBAPIError as db_err:
        logger.error("Database connection or execution failed:", db_err)
        raise db_err
    except SQLAlchemyError as exc:
        logger.error("An SQLAlchemy error occurred:", exc)
        raise exc


async def add_one(db: AsyncSession, skitour: NewSkiTourSchema):
    query = text(
        """
        INSERT INTO SKITOURS
        (user_id, date, location, elevation_gain, difficulty, description)
        VALUES(:user_id, :date, :location, :elevation_gain, :difficulty, :description)
        RETURNING *;
        """
    )
    try:
        result = await db.execute(query, skitour.dict())
        await db.commit()
        return result.fetchone()
    except DBAPIError as db_err:
        logger.error("Database connection or execution failed:", db_err)
        await db.rollback()
        raise db_err
    except SQLAlchemyError as exc:
        logger.error("An SQLAlchemy error occurred:", exc)
        raise exc


async def update_one(db: AsyncSession, skitour_uid, updated_skitour: UpdatedSkiTourSchema):
    query = text(
        """
            UPDATE SKITOURS
            SET user_id = :user_id,
                date = :date,
                location = :location,
                elevation_gain = :elevation_gain,
                difficulty = :difficulty,
                description = :description
            WHERE id = :skitour_uid
            RETURNING *;
        """
    )
    updated_skitour = updated_skitour.dict()
    parameters = {
        "user_id": updated_skitour["user_id"],
        "date": updated_skitour["date"],
        "location": updated_skitour["location"],
        "elevation_gain": updated_skitour["elevation_gain"],
        "difficulty": updated_skitour["difficulty"],
        "description": updated_skitour["description"],
        "skitour_uid": skitour_uid,  # Ensure this is correctly passed
    }
    try:
        result = await db.execute(query, parameters)
        await db.commit()
        return result.fetchone()
    except DBAPIError as db_err:
        logger.error("Database connection or execution failed:", db_err)
        await db.rollback()
        raise db_err
    except SQLAlchemyError as exc:
        await db.rollback()
        raise exc


# def delete_one(db: AsyncSession, ascent_id):
#     pass
#
#
# def edit_one(db: AsyncSession, ascent_id):
#     pass
#
#
# def is_entry_exists(db: AsyncSession, ascent_id):
#     pass
