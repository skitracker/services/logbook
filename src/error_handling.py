from fastapi.responses import JSONResponse
from sqlalchemy.exc import SQLAlchemyError
from starlette.requests import Request

from src.main import app


class ResourceNotFoundError(Exception):
    def __init__(self, resource_type: str, identifier: str):
        self.message = f"{resource_type} with ID {identifier} not found."
        super().__init__(self.message)


@app.exception_handler(ResourceNotFoundError)
async def resource_not_found_handler(request: Request, exc: ResourceNotFoundError):
    return JSONResponse(status_code=404, content={"detail": exc.message})


@app.exception_handler(SQLAlchemyError)
async def sqlalchemy_exception_handler(request: Request, exc: SQLAlchemyError):
    return JSONResponse(status_code=500, content={"detail": "An unexpected error occurred with the database."})
