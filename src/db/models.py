from sqlalchemy import Column, Date, Float, Integer, MetaData, String, Table, text
from sqlalchemy.dialects.postgresql import UUID

metadata = MetaData()

skitours = Table(
    "skitours",
    metadata,
    Column("id", UUID(as_uuid=True), primary_key=True, server_default=text("gen_random_uuid()")),
    Column("user_id", Integer, nullable=False),
    Column("date", Date, nullable=False),
    Column("location", String, nullable=False),
    Column("elevation_gain", Float, nullable=False),
    Column("difficulty", String, nullable=False),
    Column("description", String, nullable=True),
)
