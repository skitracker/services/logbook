from typing import Generator

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from src.db.database import AsyncSessionLocal


async def get_db() -> Generator[AsyncSession, None, None]:
    async with AsyncSessionLocal() as db:
        try:
            yield db
        except SQLAlchemyError as exc:
            await db.rollback()
            raise exc
        finally:
            await db.close()
