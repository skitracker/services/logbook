from functools import wraps

from fastapi import HTTPException, status
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from src.config import db_settings
from src.logger import get_logger

logger = get_logger(__name__)


def configure_async_engine():
    postgres_dsn = db_settings.POSTGRES_DNS.unicode_string()
    postgresql_engine = create_async_engine(
        postgres_dsn,
        pool_size=db_settings.POSTGRES_POOL_SIZE,
        max_overflow=db_settings.POSTGRES_MAX_OVERFLOW,
        pool_recycle=db_settings.POSTGRES_POOL_RECYCLE,
        echo=db_settings.POSTGRES_ECHO,
        future=db_settings.POSTGRES_FUTURE,
    )
    return postgresql_engine


engine = configure_async_engine()


AsyncSessionLocal = sessionmaker(
    bind=engine, class_=AsyncSession, expire_on_commit=False, autocommit=False, autoflush=False
)


def handle_db_errors(endpoint):
    @wraps(endpoint)
    async def wrapper(*args, **kwargs):
        try:
            return await endpoint(*args, **kwargs)
        except SQLAlchemyError as exc:
            logger.error(f"Database operation failed: {exc}")
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal server error")

    return wrapper
