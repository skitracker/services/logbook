import logging
from uuid import UUID

from fastapi import APIRouter, Depends, HTTPException, status
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.ext.asyncio import AsyncSession

from src.api.schemas import NewSkiTourSchema, SkiTourSchema, UpdatedSkiTourSchema
from src.crud import add_one, fetch_all, fetch_one, update_one
from src.db.session import get_db

skitours = APIRouter(prefix="/skitours", tags=["skitours"])


@skitours.get(path="/", response_model=list[SkiTourSchema], status_code=status.HTTP_200_OK)
async def index(db: AsyncSession = Depends(get_db), skip: int = 0, limit: int = 100):
    try:
        entries = await fetch_all(db=db, skip=skip, limit=limit)
        if not entries:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No entries found")
        return entries
    except SQLAlchemyError as exc:
        logging.error(f"Database query failed with {exc}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal Server Error")


@skitours.post(path="/", response_model=SkiTourSchema, status_code=status.HTTP_201_CREATED)
async def add(skitour: NewSkiTourSchema, db: AsyncSession = Depends(get_db)):
    try:
        return await add_one(db=db, skitour=skitour)
    except SQLAlchemyError as exc:
        logging.error(f"SkiTour was not added due to {exc}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal Server Error")


@skitours.get(path="/{skitour_uid}", response_model=SkiTourSchema, status_code=status.HTTP_200_OK)
async def get_one(skitour_uid: UUID, db: AsyncSession = Depends(get_db)):
    try:
        skitours = await fetch_one(db=db, skitour_uid=skitour_uid)
        if skitours is None:
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="No skitours found")
        return skitours
    except SQLAlchemyError as exc:
        logging.error(f"Database query failed with {exc}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal Server Error")


@skitours.post(path="/{skitour_uid}", response_model=UpdatedSkiTourSchema, status_code=status.HTTP_200_OK)
def update(skitour_uid: UUID, updated_skitour: UpdatedSkiTourSchema, db: AsyncSession = Depends(get_db)):
    try:
        return update_one(db=db, skitour_uid=skitour_uid, updated_skitour=updated_skitour)
    except SQLAlchemyError as exc:
        logging.error(f"Database query failed with {exc}")
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Internal Server Error")
