import uuid
from datetime import date
from typing import Optional

from pydantic import BaseModel


class SkiTourBaseSchema(BaseModel):
    """
    Base schema for Ski Tour with common fields.
    """

    user_id: int
    date: date
    location: str
    elevation_gain: float
    difficulty: str
    description: Optional[str] = None

    class Config:
        extra = "forbid"


class NewSkiTourSchema(SkiTourBaseSchema):
    """
    Schema for creating a new Ski Tour.
    """

    pass


class UpdatedSkiTourSchema(SkiTourBaseSchema):
    """
    Schema for updating an existing Ski Tour.
    """

    pass


class SkiTourSchema(SkiTourBaseSchema):
    """
    Schema for Ski Tour with an additional ID field.
    """

    id: uuid.UUID
