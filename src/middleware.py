import time

from starlette.middleware.base import BaseHTTPMiddleware
from starlette.requests import Request

from .logger import get_logger

logger = get_logger(__name__)


class LogMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request: Request, call_next):
        start_time = time.time()
        response = await call_next(request)
        process_time = time.time() - start_time
        logger.info(
            f"Request: {request.method} {request.url.path} - Completed in {process_time} seconds with status {response.status_code}"
        )
        return response
