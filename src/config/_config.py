import logging
import os
from functools import lru_cache

from pydantic import PostgresDsn
from pydantic_core._pydantic_core import ValidationError
from pydantic_settings import BaseSettings, SettingsConfigDict

logger = logging.getLogger(__name__)
# TODO
DOTENV = os.path.join(os.path.dirname(__file__), "..", "..", ".env")


class BaseProjectSettings(BaseSettings):
    model_config = SettingsConfigDict(env_file=DOTENV, env_file_encoding="utf-8", extra="ignore")
    logging_level: str = "INFO"


class PostgreSqlSettings(BaseProjectSettings):
    POSTGRES_DNS: PostgresDsn
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DB: str
    POSTGRES_POOL_SIZE: int = 1
    POSTGRES_MAX_OVERFLOW: int = 10
    POSTGRES_POOL_RECYCLE: int = 3600
    POSTGRES_ECHO: bool = False
    POSTGRES_FUTURE: bool = True


class AppSettings(BaseProjectSettings):
    pass


@lru_cache
def get_db_settings() -> PostgreSqlSettings:
    try:
        return PostgreSqlSettings()
    except ValidationError as exc:
        logger.error(f"Error loading settings: {exc}")
        raise


db_settings = get_db_settings()

if __name__ == "__main__":
    print(f"Settings model config: {db_settings.model_config}")
    print(f"Settings model dump: {db_settings.model_dump()}")
