import random
from datetime import date

from faker import Faker

from src.api.schemas import NewSkiTourSchema
from src.crud import add_one
from src.db.session import get_db

today = date.today()

fake = Faker()
fake.seed_instance(4321)


def generate_date_between(start_date=date(2019, 9, 27), end_date=date.today()):
    return fake.date_between(start_date, end_date)


def generate_peak_name():
    prefixes = ["Peak", "Summit", "Pinnacle", "Spire", "Crag"]
    suffixes = ["Top", "Point", "Summit", "Horn", "Butte"]
    peak_elements = [fake.last_name(), fake.city(), fake.word().capitalize()]

    prefix = random.choice(prefixes)
    suffix = random.choice(suffixes)
    element = random.choice(peak_elements)

    name_structure = random.choice([f"{prefix} {element}", f"{element} {suffix}", f"{prefix} {element} {suffix}"])

    return name_structure


def generate_number_between(start=1, end=8848):
    return random.randint(start, end)


def generate_difficulty():
    fake_number = random.randint(1, 6)
    suffix = ["+", "-"]
    fake_suffix = random.choice(suffix)
    name_structure = random.choice([f"{fake_number}", f"{fake_number}{fake_suffix}"])

    return name_structure


def generate_random_sentence():
    return fake.sentence()


looper = 10

for _ in range(looper):
    ascent_data = {
        "user_id": generate_number_between(1, 1000),
        "date": generate_date_between(),
        "location": generate_peak_name(),
        "elevation_gain": generate_number_between(),
        "difficulty": generate_difficulty(),
        "description": generate_random_sentence(),
    }

    ascent = NewSkiTourSchema(**ascent_data)

    with next(get_db()) as db:
        result = add_one(db, ascent)
        print(result)
